-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 21, 2022 at 11:03 AM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `complexscenarios210521`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(28, 'Balaji', 'docbalaji1@gmail.com', 'Coimbatore ', 'Sri Ramakrishna hospital ', NULL, NULL, '2021/05/25 08:04:11', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(29, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/05/25 15:55:42', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(30, 'sriram', 'srivats.007.2003@gmail.com', 'CHENNAI', 'SRM', NULL, NULL, '2021/05/25 17:43:28', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(31, 'Jebin John', 'jebin_j.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/05/25 18:05:43', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(32, 'ROHIT ARJUNAN', 'a.rohit@abbott.com', 'Coimbatore', 'AV', NULL, NULL, '2021/05/25 18:06:46', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(33, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/05/25 18:17:28', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(34, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/05/25 18:18:39', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(35, 'Dr S Balaji', 'docbalaji1@gmail.com', 'Coimbatore', 'Sri Ramakrishna Hospital', NULL, NULL, '2021/05/25 18:31:58', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(36, 'ROHIT ARJUNAN', 'a.rohit@abbott.com', 'Coimbatore', 'AV', NULL, NULL, '2021/05/25 18:32:36', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(37, 'Dr Arvind Sharma ', 'drarvindsharma1977@gmail.com', 'vadodara', 'Rhythm heart institute, vadodara ', NULL, NULL, '2021/05/25 18:47:42', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(38, 'prem krishna', 'premkrishna2k1@gmail.com', 'coimbatore', 'psg ssh', NULL, NULL, '2021/05/25 18:48:43', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(39, 'ROHIT ARJUNAN', 'a.rohit@abbott.com', 'Coimbatore', 'AV', NULL, NULL, '2021/05/25 18:49:58', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(40, 'Dr Arvind sharma', 'drarvindsharma1977@gmail.com', 'vadodara ', 'Rhythm heart institute, vadodara ', NULL, NULL, '2021/05/25 18:52:41', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(41, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/05/25 18:54:16', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(42, 'DR ARUN KAUSHIK P', 'drarunkaushik@gmail.com', 'COIMBATORE', 'PSG SSH', NULL, NULL, '2021/05/25 18:55:09', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(43, 'sriram', 'srivats.007.2003@gmail.com', 'CHENNAI', 'SRM', NULL, NULL, '2021/05/25 18:55:28', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(44, 'ARUN KAUSHIK P', 'drarunkaushik@gmail.com', 'COIMBATORE', 'PSG SSH', NULL, NULL, '2021/05/25 18:56:47', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(45, 'Govind', 'govindaraj.kamaraj@abbott.com', 'Chennai ', 'Abbott Vascular ', NULL, NULL, '2021/05/25 18:59:38', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(46, 'SNB', 'drsnboopathy@gmail.com', 'Chennai', 'SRIHER', NULL, NULL, '2021/05/25 19:01:59', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(47, 'Nagendra Boopathy Senguttuvan', 'drsnboopathy@gmail.com', 'Chennai', 'SRIHER', NULL, NULL, '2021/05/25 19:39:08', '2021-05-25', '2021-05-25', 1, 'Abbott'),
(48, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/06/17 11:40:07', '2021-06-17', '2021-06-17', 1, 'Abbott'),
(49, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/06/17 11:59:26', '2021-06-17', '2021-06-17', 1, 'Abbott'),
(50, 'Lijo Lalu ', 'lijo.k@abott.com', 'cochin', 'Abbott', NULL, NULL, '2021/06/18 18:39:33', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(51, 'Lijo lalu K', 'lijo.k@abbott.com', 'cochin', 'Abbott', NULL, NULL, '2021/06/18 18:41:08', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(52, 'Dr. Navin Mathew', 'nischalnh8825@gmail.com', 'Kochi', 'Amrita Institute Of Medical Sciences', NULL, NULL, '2021/06/18 18:43:57', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(53, 'Dr Praveen G Pai', 'praveen.pai.g@gmail.com', 'Kochi', 'Amrita Institute of medical sciences', NULL, NULL, '2021/06/18 18:47:40', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(54, 'Sudheesh Chandran', 'sudheesh.chandran@abbott.com', 'Trivandrum', 'Abbott', NULL, NULL, '2021/06/18 18:52:12', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(55, 'Dr Praveen G Pai', 'praveen.pai.g@gmail.com', 'Kochi', 'Amrita Institute of medical sciences', NULL, NULL, '2021/06/18 18:52:59', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(56, 'Rajiv C', 'rajivchandran66@gmail.com', 'Kochi', 'Amrita', NULL, NULL, '2021/06/18 18:53:35', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(57, 'K U Natarajan', 'kunatarajan@gmail.com', 'Kochi', 'AIMS', NULL, NULL, '2021/06/18 18:54:11', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(58, 'Stalin Roy', 'mailstalin@gmail.com', 'Kollam', 'MEDITRINA ', NULL, NULL, '2021/06/18 18:55:40', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(59, 'Santhosh Chandran', 'santhosh.chandran@abbott.com', 'Ernakulam', 'Abbott Vascular', NULL, NULL, '2021/06/18 19:02:00', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(60, 'Dr Praveen G Pai', 'praveen.pai.g@gmail.com', 'Kochi', 'Amritha Hospital', NULL, NULL, '2021/06/18 19:02:38', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(61, 'Stalin Roy', 'mailstalin@gmail.com', 'Kollam', 'MEDITRINA ', NULL, NULL, '2021/06/18 19:02:43', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(62, 'RAJESH THACHATHODIYL', 'rajesht67@gmail.com', 'ALUVA', 'aims kochi', NULL, NULL, '2021/06/18 19:06:44', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(63, 'Santhosh ', 'santhosh.chandran@abbott.com', 'Kochi', 'Abbott', NULL, NULL, '2021/06/18 19:53:05', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(64, 'Santosh Chandran', 'santhosh.chandran@abbott.com', 'Kochi', 'Abbott', NULL, NULL, '2021/06/18 19:54:07', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(65, 'Santhosh Chandran', 'santhosh.chandran@abbott.com', 'Ernakulam', 'Abbott Vascular', NULL, NULL, '2021/06/18 20:18:10', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(66, 'Santhosh Chandran', 'santhosh.chandran@abbott.com', 'Ernakulam', 'Abbott Vascular', NULL, NULL, '2021/06/18 20:22:22', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(67, 'Deepak', 'deepakdavidson@yahoo.com', 'Kottayam', 'Caritas Hospital', NULL, NULL, '2021/06/18 20:27:42', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(68, 'Santhosh Chandran', 'santhosh.chandran@abbott.com', 'Ernakulam', 'Abbott Vascular', NULL, NULL, '2021/06/18 21:03:13', '2021-06-18', '2021-06-18', 1, 'Abbott'),
(69, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/06/24 10:42:36', '2021-06-24', '2021-06-24', 1, 'Abbott'),
(70, 'rohit', 'a.rohit@abbott.com', 'Coimbatore', 'AV', NULL, NULL, '2021/06/25 18:04:04', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(71, 'BIVIN WILSON', 'bivwilson@gmail.com', 'COIMBATORE', 'GKNM HOSPITAL', NULL, NULL, '2021/06/25 18:14:47', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(72, 'MOHAN MANI ', 'dr.mohan1978@yahoo.co.in', 'coimbatore', 'KMCH Heart Institute', NULL, NULL, '2021/06/25 18:26:34', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(73, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/06/25 18:43:04', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(74, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/06/25 18:43:17', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(75, 'Dr.V.Nandhakumar', 'nandhacard2013@gmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/25 18:48:33', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(76, 'BIVIN WILSON', 'bivwilson@gmail.com', 'COIMBATORE', 'GKNM HOSPITAL', NULL, NULL, '2021/06/25 18:54:03', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(77, 'MADHESWARAN', 'drmadheswaran@gmail.com', 'Coimbatore', 'SRH', NULL, NULL, '2021/06/25 18:56:21', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(78, 'REFAI SHOWKATHALI', 'chennaiheartdoc@gmail.com', 'CHENNAI', 'Apollo', NULL, NULL, '2021/06/25 19:00:15', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(79, 'Aashish ', 'chopraaashish@hotmail.com', 'Chennai ', 'MMM', NULL, NULL, '2021/06/25 19:07:31', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(80, 'Dr K P Suresh Kumar', 'kpscardio@yahoo.co.in', 'Chennai', 'Kauvery Hospital', NULL, NULL, '2021/06/25 19:45:53', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(81, 'Aashish Chopra ', 'chopraaashish@hotmail.com', 'Chennai ', 'MMM', NULL, NULL, '2021/06/25 20:07:33', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(82, 'Mohan mani ', 'dr.mohan1978@yahoo.co.in', 'Coimbatore ', 'KMCH ', NULL, NULL, '2021/06/25 21:14:25', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(83, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/06/25 21:33:40', '2021-06-25', '2021-06-25', 1, 'Abbott'),
(84, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/06/26 13:10:54', '2021-06-26', '2021-06-26', 1, 'Abbott'),
(85, 'Dilip Ratnani', 'dilipratnani@yahoo.com', 'BHILAI', 'SSIMS', NULL, NULL, '2021/06/27 03:56:49', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(86, 'Rahul Kolhekar', 'rahul.vkolhekar@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/06/27 11:05:37', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(87, 'Dr.javed ali khan', 'drjavedakhan@yahoo.com', 'Raipur', 'Ramkrishna Care Hospital', NULL, NULL, '2021/06/27 11:35:02', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(88, 'DR K GURUNATH', 'drkgurunathhospital@gmail.com', 'BHILAI', 'K GURUNATH HOSPITAL', NULL, NULL, '2021/06/27 11:39:37', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(89, 'Dr Jogesh Vishandasani', 'jogesh.gmcngp@gmail.com', 'Raipur', 'Pt JNM Medical College, Raipur', NULL, NULL, '2021/06/27 11:46:51', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(90, 'Dr Prathap Kumar', 'prathapndr@gmail.com', 'Kollam', 'Meditrina Hospital', NULL, NULL, '2021/06/27 11:47:07', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(91, 'Dr Prathap Kumar N', 'prathapndr@gmail.com', 'Kollam', 'Meditrina Hospital', NULL, NULL, '2021/06/27 11:47:55', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(92, 'DR.SATISH SURYAVANSHI', 'DRSATISH_SURYAVANSHI@YAHOO.COM', 'RAIPUR', 'SMC HEART INSTITUTE AND IVF RESEARCH CENTRE RAIPUR', NULL, NULL, '2021/06/27 11:48:12', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(93, 'Dr Prathap Kumar N', 'prathapndr@gmail.com', 'Kollam', 'Meditrina Hospital', NULL, NULL, '2021/06/27 11:52:01', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(94, 'Atanu Banerjee ', 'atanu.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/06/27 11:52:39', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(95, 'MAHENDRA SAMAL', 'DRSAMAL2000@YAHOO.COM', 'BILASPUR', 'APOLLO HOSPITAL', NULL, NULL, '2021/06/27 12:01:08', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(96, 'MAHENDRA SAMAL', 'DRSAMAL2000@YAHOO.COM', 'BILASPUR', 'APOLLOHOSPITAL', NULL, NULL, '2021/06/27 12:09:42', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(97, 'dilip ratnani', 'drdilipratnani@gmail.com', 'bhilai', 'SSIMS ', NULL, NULL, '2021/06/27 12:10:11', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(98, 'Prof Smit Shrivastva ', 'dr.smit.shrivastava@gmail.com', 'Raipur', 'Cardiology Clinic ', NULL, NULL, '2021/06/27 12:10:49', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(99, 'DR.SATISH SURYAVANSHI', 'DRSATISH_SURYAVANSHI@YAHOO.COM', 'RAIPUR', 'SMC HEART INSTITUTE AND IVF RESEARCH CENTRE RAIPUR', NULL, NULL, '2021/06/27 12:13:09', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(100, 'DR K GURUNATH', 'drkgurunathhospital@gmail.com', 'BHILAI', 'K GURUNATH HOSPITAL', NULL, NULL, '2021/06/27 12:27:50', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(101, 'DR K GURUNATH', 'drkgurunathhospital@gmail.com', 'BHILAI', 'K GURUNATH HOSPITAL', NULL, NULL, '2021/06/27 12:30:18', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(102, 'DR K GURUNATH', 'drkgurunathhospital@gmail.com', 'BHILAI', 'K GURUNATH HOSPITAL', NULL, NULL, '2021/06/27 12:32:02', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(103, 'dilip ratnani', 'drdilipratnani@gmail.com', 'bhilai', 'SSIMS ', NULL, NULL, '2021/06/27 12:38:23', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(104, 'DR K GURUNATH', 'drkgurunathhospital@gmail.com', 'BHILAI', 'K GURUNATH HOSPITAL', NULL, NULL, '2021/06/27 12:41:40', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(105, 'Dilip ratnani ', 'drdilipratnani@gmail.com', 'Bhilai', ' Ssims', NULL, NULL, '2021/06/27 12:48:38', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(106, 'dilip ratnani', 'drdilipratnani@gmail.com', 'bhilai', 'SSIMS ', NULL, NULL, '2021/06/27 12:52:11', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(107, 'dilip ratnani', 'dilipratnani@yahoo.com', 'bhilai', 'ssims', NULL, NULL, '2021/06/27 13:01:01', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(108, 'Prof Smit Shrivastva ', 'dr.smit.shrivastava@gmail.com', 'Raipur', 'Cardiology Clinic ', NULL, NULL, '2021/06/27 13:16:11', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(109, 'Prof Smit Shrivastva ', 'dr.smit.shrivastava@gmail.com', 'Raipur', 'Cardiology Clinic ', NULL, NULL, '2021/06/27 13:38:04', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(110, 'Prof Smit Shrivastva ', 'dr.smit.shrivastava@gmail.com', 'Raipur', 'Cardiology Clinic ', NULL, NULL, '2021/06/27 13:41:22', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(111, 'DR K GURUNATH', 'drkgurunathhospital@gmail.com', 'BHILAI', 'K GURUNATH HOSPITAL', NULL, NULL, '2021/06/27 14:05:42', '2021-06-27', '2021-06-27', 1, 'Abbott'),
(112, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/07/05 22:13:54', '2021-07-05', '2021-07-05', 1, 'Abbott'),
(113, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/07/06 09:23:25', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(114, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/07/06 09:25:53', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(115, 'Govind', 'govindaraj.kamaraj@abbott.com', 'Chennai', 'Abbott Vascular', NULL, NULL, '2021/07/06 12:53:33', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(116, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/07/06 19:02:10', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(117, 'Govind', 'govindaraj.kamaraj@abbott.com', 'Chennai', 'Abbott Vascular', NULL, NULL, '2021/07/06 19:10:18', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(118, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021/07/06 19:15:16', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(119, 'CHANDRADSEKARAN KOLANDAISAMY', 'kcpscg@gmail.com', 'chennai', 'Prashanth superspeciality hospital', NULL, NULL, '2021/07/06 19:16:37', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(120, 'lawrance jesuraj', 'drjesuraj@gmail.com', 'COIMBATORE', 'kmch hospitals', NULL, NULL, '2021/07/06 19:27:13', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(121, 'K.Jaishankar', 'drkjs68@yahoo.com', 'Chennai', 'MIOT', NULL, NULL, '2021/07/06 19:28:16', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(122, 'Karthigesan Dr AM', 'amkgesan@hotmail.com', 'chennai', 'Apollo', NULL, NULL, '2021/07/06 19:31:23', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(123, 'Dr T R uralidharan', 'muralidharantr@gmail.com', 'Chennai', 'SRIHER', NULL, NULL, '2021/07/06 19:32:03', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(124, 'KARTHIGESAN A M', 'amkgesan@hotmail.com', 'chennai', 'apollo', NULL, NULL, '2021/07/06 19:38:28', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(125, 'DR.GURU', 'DRGURU73@YAHOO.COM', 'CHENNAI', 'MIOtT', NULL, NULL, '2021/07/06 19:39:45', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(126, 'GURU', 'DRGURU73@YAHOO.COM', 'CHENNAI', 'MIOT', NULL, NULL, '2021/07/06 19:48:34', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(127, 'GURU', 'JJNN@YAHOO.COM', 'KKK@YAHOO.COM', 'KKK', NULL, NULL, '2021/07/06 21:18:14', '2021-07-06', '2021-07-06', 1, 'Abbott'),
(128, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/07/17 13:37:43', '2021-07-17', '2021-07-17', 1, 'Abbott'),
(129, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/07/17 13:40:44', '2021-07-17', '2021-07-17', 1, 'Abbott'),
(130, 'S Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/07/18 11:12:41', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(131, 'S Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/07/18 12:10:05', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(132, 'Subhabrata', 'subhabrata.banerjee@abbott.com', 'kolkata', 'av', NULL, NULL, '2021/07/18 18:11:28', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(133, 'Rahul Kolhekar', 'rahul.vkolhekar@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021/07/18 18:32:50', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(134, 'Sachit Roy', 'sachit.roy@abbott.com', 'Patna', 'AV', NULL, NULL, '2021/07/18 18:36:16', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(135, 'Sachit Roy', 'sachit.roy@abbott.com', 'Patna', 'AV', NULL, NULL, '2021/07/18 18:38:01', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(136, 'DIBYA RANJAN BEHERA', 'drdibya26@gmail.com', 'Bhubaneswar', 'SUM', NULL, NULL, '2021/07/18 18:53:38', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(137, 'Suvro Banerjee', 'drsuvrob@gmail.com', 'Kolkata', 'AGHL', NULL, NULL, '2021/07/18 18:54:42', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(138, 'Dr Raja Nag', 'dr.raja.nag@gmail.com', 'Kolkata', 'Apollo Gleneagles hospitals', NULL, NULL, '2021/07/18 18:55:59', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(139, 'Dr Prakas Chandra Mondal', 'drpcm.cardio@gmail.com', 'Kolkata', 'Apollo Multispeciality Hospital', NULL, NULL, '2021/07/18 18:58:29', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(140, 'Dr. Priyam Mukherjee', 'drpriyam.mukherjee@gmail.com', 'Barrackpore', 'Fortis', NULL, NULL, '2021/07/18 19:00:17', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(141, 'shuvanan ray', 'shuvananray@gmail.com', 'kolkata', 'fortis', NULL, NULL, '2021/07/18 19:02:47', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(142, 'Dhananjay kumar ', 'djvishainya@gmail.com', 'Sasaram', 'Nmch jamuhar', NULL, NULL, '2021/07/18 19:06:59', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(143, 'Raja Nag', 'dr.raja.nag@gmail.com', 'Kolkata ', 'Apollo gleneagles hospital ', NULL, NULL, '2021/07/18 20:18:27', '2021-07-18', '2021-07-18', 1, 'Abbott'),
(144, 'Subhabrata', 'subhabrata.banerjee@abbott.com', 'kolkata', 'av', NULL, NULL, '2021/07/18 20:22:01', '2021-07-18', '2021-07-18', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(49, 'arohit', 'a.rohit@abbott.com', 'Coimbatore ', 'AV', NULL, NULL, '2021-05-24 21:33:12', '2021-05-24 21:33:12', '2021-05-24 23:03:12', 0, 'Abbott', 'db92a4682d7b1ca274e909a78ce043a7'),
(50, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-25 11:31:38', '2021-05-25 11:31:38', '2021-05-25 13:01:38', 0, 'Abbott', 'fc04b95f8429b07d74f86b257d1e3a25'),
(51, 'Prithiviraj', 'sumiprithivi97@gmail.com', 'Coddalour', 'Surendra', NULL, NULL, '2021-05-25 13:32:00', '2021-05-25 13:32:00', '2021-05-25 15:02:00', 0, 'Abbott', '5704b1c9ef1b04df65bd63140bbe4f2e'),
(52, 'sriram', 'srivats.007.2003@gmail.com', 'CHENNAI', 'SRM', NULL, NULL, '2021-05-25 17:42:54', '2021-05-25 17:42:54', '2021-05-25 19:12:54', 0, 'Abbott', '709c95c29d7288897e9fd5142a6b140b'),
(53, 'SNB', 'drsnboopathy@gmail.com', 'Chennai', 'SRIHER', NULL, NULL, '2021-05-25 18:46:18', '2021-05-25 18:46:18', '2021-05-25 19:01:15', 0, 'Abbott', 'e457a1a5c8e88107e2fae7c6ad49f17c'),
(54, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-25 18:58:35', '2021-05-25 18:58:35', '2021-05-25 20:28:35', 1, 'Abbott', 'd6a47843ad94d4c57902c1c10adb4599'),
(55, 'Prasanna Murugesan', 'drprasanna98@yahoo.com', 'Hosur', 'Kauvery hospital', NULL, NULL, '2021-05-25 19:03:18', '2021-05-25 19:03:18', '2021-05-25 20:33:18', 1, 'Abbott', '27d1303ab2955e4ca1abf8d1393dc1e9'),
(56, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-25 19:05:42', '2021-05-25 19:05:42', '2021-05-25 20:35:42', 1, 'Abbott', '56c1fdf1098e9a53a387c541acc4edc1'),
(57, 'Govind ', 'govindaraj.kamaraj@abbott.com', 'Chennai ', 'Abbott Vascular ', NULL, NULL, '2021-05-25 19:07:05', '2021-05-25 19:07:05', '2021-05-25 20:37:05', 1, 'Abbott', 'f192a4bc5e0f65f05fc33c23bbf043d5'),
(58, 'Kirubakaran ', 'kirubakarankk@gmail.com', 'Chennai ', 'Apollo', NULL, NULL, '2021-05-25 19:08:30', '2021-05-25 19:08:30', '2021-05-25 20:38:30', 1, 'Abbott', '9e1d1761345ed89929c9d7e2ef678cce'),
(59, 'Ashok', 'gashokmkumar@gmail.com', 'Chennai', 'Chettinad', NULL, NULL, '2021-05-25 19:09:12', '2021-05-25 19:09:12', '2021-05-25 20:39:12', 1, 'Abbott', '54f11e69345ef18710ba3d9414b84cae'),
(60, 'Radha', 'Radhapri@gmail.com', 'Chennai ', 'Apollo', NULL, NULL, '2021-05-25 19:09:38', '2021-05-25 19:09:38', '2021-05-25 20:39:38', 1, 'Abbott', '33abba2ac3fc90dea4ecfd0757d063cf'),
(61, 'Giridharan ', 'Girigirias@gmail.con', 'Pondicherry ', 'Pondy', NULL, NULL, '2021-05-25 19:10:01', '2021-05-25 19:10:01', '2021-05-25 20:40:01', 1, 'Abbott', '4909f2b1546ffc699eb79c7657f5ecd5'),
(62, 'Boopathi', 'aboopathia@gmail.com', 'Namkkal', 'Vmch ', NULL, NULL, '2021-05-25 19:10:12', '2021-05-25 19:10:12', '2021-05-25 20:40:12', 1, 'Abbott', '7a0b02da1b84fd89adc69e9deb6d4594'),
(63, 'Joseph', 'Josephtheo84@gmail.com', 'Trichy', 'Kauvery', NULL, NULL, '2021-05-25 19:11:11', '2021-05-25 19:11:11', '2021-05-25 20:41:11', 0, 'Abbott', 'f79ec3e207d0772a06f0f2f57bf2631e'),
(64, 'Avinash', 'Avinbox@yahoo.com', 'Pondicherry ', 'JIPMER', NULL, NULL, '2021-05-25 19:11:44', '2021-05-25 19:11:44', '2021-05-25 20:41:44', 0, 'Abbott', '3602e7d723f10e041a48fcef2b4ff72f'),
(65, 'Shiyam', 'shiyam.sundar@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-05-25 19:12:09', '2021-05-25 19:12:09', '2021-05-25 20:42:09', 0, 'Abbott', '43c404d3ec1bf792d47d338ff662c1a9'),
(66, 'Chennappan vetri', 'chennaappan32@gmail.com', 'Salem', 'Sks hospital', NULL, NULL, '2021-05-25 19:12:25', '2021-05-25 19:12:25', '2021-05-25 19:28:24', 0, 'Abbott', '35de5e9603bb50ec57972547c01ddf04'),
(67, 'Ganesh', 'drganeshchc@gmail.com', 'Chennai ', 'SRM', NULL, NULL, '2021-05-25 19:12:29', '2021-05-25 19:12:29', '2021-05-25 20:42:29', 0, 'Abbott', '71fecb104d61ff6a209f6a0ffd44d593'),
(68, 'Priyadharsan', 'priyadharsantech97@gmail.com', 'Coimbatore', 'GKNMH', NULL, NULL, '2021-05-25 19:12:31', '2021-05-25 19:12:31', '2021-05-25 20:42:31', 0, 'Abbott', '59b742dd7579e546e876aeda7a343052'),
(69, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-05-25 19:13:17', '2021-05-25 19:13:17', '2021-05-25 19:13:27', 0, 'Abbott', 'f80091c37a6ceadf5aa9b3fa0853bde9'),
(70, 'Lavanya', 'lavi4321@gmail.com', 'Chennai', 'Chennai', NULL, NULL, '2021-05-25 19:14:11', '2021-05-25 19:14:11', '2021-05-25 20:44:11', 0, 'Abbott', 'f330db951054b9afacd23e9d7ffa440a'),
(71, 'Sundar', 'Sundarsaivim@gmail.com', 'Chennai ', 'Kauvery', NULL, NULL, '2021-05-25 19:14:37', '2021-05-25 19:14:37', '2021-05-25 20:44:37', 0, 'Abbott', 'bf17d41c49dc050d0c5fb83c591e8153'),
(72, 'Asuthosh', 'Asutoshmd@gmail.com', 'Pondicherry ', 'JIPMER', NULL, NULL, '2021-05-25 19:15:05', '2021-05-25 19:15:05', '2021-05-25 20:45:05', 0, 'Abbott', '75c658486492fe8b1abdabc6f15b6636'),
(73, 'Dr T R Nanda Kumar', 'drnkheart@gmail.com', 'Coimbatore', 'Sri Ramakrishna Hospital', NULL, NULL, '2021-05-25 19:15:58', '2021-05-25 19:15:58', '2021-05-25 19:20:35', 0, 'Abbott', '9839d79a1785b5ebaa0930fc80542467'),
(74, 'SHAJAHAN', 'shajahan8121997@gmail.com', 'Kallakurichi', 'Sri Raju hospital', NULL, NULL, '2021-05-25 19:16:05', '2021-05-25 19:16:05', '2021-05-25 20:46:05', 0, 'Abbott', '5ec5d22e2a9ba394496bc003f6630392'),
(75, 'Raman', 'raman24398@gmail.com', 'Pollachi', 'Arun hospital', NULL, NULL, '2021-05-25 19:16:06', '2021-05-25 19:16:06', '2021-05-25 20:46:06', 0, 'Abbott', 'be02e95fba8ccf8a0df6fa946628ba0a'),
(76, 'Immanuel', 'immanueldrdm@gmail.com', 'Chennai', 'Sugam', NULL, NULL, '2021-05-25 19:16:10', '2021-05-25 19:16:10', '2021-05-25 20:46:10', 0, 'Abbott', 'ad06a8d644d134fcd739bf2c1c7e785b'),
(77, 'Jesu', 'kjdosstvl@gmail', 'Cbe', 'Psg', NULL, NULL, '2021-05-25 19:17:09', '2021-05-25 19:17:09', '2021-05-25 20:36:43', 0, 'Abbott', '8351af6981a7e951a6f8358907d0893e'),
(78, 'Raman', 'raman24398@gmail.com', 'Pollachi', 'Arun hospital', NULL, NULL, '2021-05-25 19:17:15', '2021-05-25 19:17:15', '2021-05-25 20:47:15', 0, 'Abbott', '7b32458d475ddacdb0ce2159a0736c04'),
(79, 'Prasad', 'Prasadjai@Rediffmail.com', 'Coimbatore', 'Kongunad Hospital', NULL, NULL, '2021-05-25 19:18:23', '2021-05-25 19:18:23', '2021-05-25 19:56:11', 0, 'Abbott', '44ddbbe9d89145144fec96d6e077e6c2'),
(80, 'Vijayakumaran. M', 'vijayakumaranmarimuthu@gmail.com', 'Coimbatore ', 'GKNM hospital ', NULL, NULL, '2021-05-25 19:19:32', '2021-05-25 19:19:32', '2021-05-25 20:49:32', 0, 'Abbott', '22a551356786367274b68e3faa012ff2'),
(81, 'Shalin', 'shalin.rajendrasinhsolnki@abbott.com', 'Vadodara ', 'AV', NULL, NULL, '2021-05-25 19:20:17', '2021-05-25 19:20:17', '2021-05-25 20:50:17', 0, 'Abbott', 'd476fa0f7316115a332046b80b996ffa'),
(82, 'Dr T R Nanda Kumar', 'drnkheart@gmail.com', 'Coimbatore', 'Sri Ramakrishna Hospital', NULL, NULL, '2021-05-25 19:20:51', '2021-05-25 19:20:51', '2021-05-25 20:50:51', 0, 'Abbott', '1d73c2f5d888855affbeb4a53a4538cf'),
(83, 'Boopathi', 'aboopathia@gmail.com', 'Namkkal', 'Vmch ', NULL, NULL, '2021-05-25 19:21:34', '2021-05-25 19:21:34', '2021-05-25 20:51:34', 0, 'Abbott', '888e33f067da3462e938ce6a0e02f8ab'),
(84, 'Boopathi', 'aboopathia@gmail.com', 'Namkkal', 'Vmch ', NULL, NULL, '2021-05-25 19:22:03', '2021-05-25 19:22:03', '2021-05-25 20:52:03', 0, 'Abbott', '8fe9c1d9094abde180b6ba88bc6155e2'),
(85, 'Boopathi', 'aboopathia@gmail.com', 'Namkkal', 'Vmch ', NULL, NULL, '2021-05-25 19:23:51', '2021-05-25 19:23:51', '2021-05-25 20:53:51', 0, 'Abbott', 'b088ce149788c57367267841ff35ca3a'),
(86, 'Libin D', 'libind320@gmail.com', 'Coimbatore ', 'Muthus hospital ', NULL, NULL, '2021-05-25 19:24:02', '2021-05-25 19:24:02', '2021-05-25 20:54:02', 0, 'Abbott', '2206066d1bdfe434963ebcb04b5c022c'),
(87, 'Dr T R Nanda Kumar', 'drnkheart@gmail.com', 'Coimbatore', 'Sri Ramakrishna Hospital', NULL, NULL, '2021-05-25 19:25:58', '2021-05-25 19:25:58', '2021-05-25 20:55:58', 0, 'Abbott', '448318b1af17b78e0bccd85611c197a0'),
(88, 'Harish', 'harish2356@gmail.com', 'Chennai ', 'SRM', NULL, NULL, '2021-05-25 19:27:34', '2021-05-25 19:27:34', '2021-05-25 20:57:34', 0, 'Abbott', '8c1abe144c0cd770ff8a8f3ebfff0259'),
(89, 'Avinash ', 'avinashjaya@gmail.com', 'Chennai', 'Kims', NULL, NULL, '2021-05-25 19:28:12', '2021-05-25 19:28:12', '2021-05-25 20:58:12', 0, 'Abbott', 'c1e4bc01fccc350a1fbe7b48f4122522'),
(90, 'Vijayakumaran. M', 'vijayakumaranmarimuthu@gmail.com', 'Coimbatore ', 'GKNM hospital ', NULL, NULL, '2021-05-25 19:28:33', '2021-05-25 19:28:33', '2021-05-25 20:58:33', 0, 'Abbott', '944318b587a4d65637e7916214263f3b'),
(91, 'Ramchandran', 'rampoovendiran@gmail.com', 'Salem ', 'Vims', NULL, NULL, '2021-05-25 19:31:58', '2021-05-25 19:31:58', '2021-05-25 21:01:58', 0, 'Abbott', 'ae51951a0cf907d153227f83bb1235b9'),
(92, 'Vijayakumaran. M', 'vijayakumaranmarimuthu@gmail.com', 'Coimbatore ', 'GKNM hospital ', NULL, NULL, '2021-05-25 19:36:29', '2021-05-25 19:36:29', '2021-05-25 19:37:41', 0, 'Abbott', '72fc7694c92532f14c6a54f722f8d805'),
(93, 'Vishnuprabhu ', 'drvishnumddm@yahoo.co.in', 'Salem', 'Aishwaryam', NULL, NULL, '2021-05-25 19:37:14', '2021-05-25 19:37:14', '2021-05-25 21:07:14', 0, 'Abbott', '174b37157b3773869390e4638e20892b'),
(94, 'Santosh Chandran', 'santhosh.chandran@abbott.com', 'Kochi', 'Abbott ', NULL, NULL, '2021-05-25 19:45:24', '2021-05-25 19:45:24', '2021-05-25 21:15:24', 0, 'Abbott', 'ab2d7237231ff5e31aeecfe74f6b5a72'),
(95, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-25 19:48:38', '2021-05-25 19:48:38', '2021-05-25 21:18:38', 0, 'Abbott', '4a98acf9c357298a31ccf8f8ab781ff2'),
(96, 'KC Rathan ', 'Kamanatchanolian.rathan@abbott.com', 'Chennai ', 'Av', NULL, NULL, '2021-05-25 19:51:04', '2021-05-25 19:51:04', '2021-05-25 21:21:04', 0, 'Abbott', '06f9c1233a9c1394dfa0f44f43f7c18a'),
(97, 'Daniel K', 'danielchristoper1998@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-05-25 19:54:52', '2021-05-25 19:54:52', '2021-05-25 21:24:52', 0, 'Abbott', '54082448bc90601f16b4460f0c1d746f'),
(98, 'Rajarajan', 'mrajarajan.ppy@gmail.com', 'Palakkad', 'Paalana hospital', NULL, NULL, '2021-05-25 20:00:55', '2021-05-25 20:00:55', '2021-05-25 21:30:55', 0, 'Abbott', '141bcd5a3acc566be6f9e96023ebc7eb'),
(99, 'Udit', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-05-25 20:02:41', '2021-05-25 20:02:41', '2021-05-25 20:44:39', 0, 'Abbott', 'cf6ea7e69cef3537920222d3a89edaa9'),
(100, 'Vikash', 'vikash0089@gmail.com', 'Chennai ', 'SRMC', NULL, NULL, '2021-05-25 20:03:14', '2021-05-25 20:03:14', '2021-05-25 21:33:14', 0, 'Abbott', 'cad74a52c1c1edfde7b633620b941cd1'),
(101, 'Sivakumar', 'shiva78.r@gmail.com', 'Chennai ', 'Billroth ', NULL, NULL, '2021-05-25 20:06:04', '2021-05-25 20:06:04', '2021-05-25 21:36:04', 0, 'Abbott', '5b6ae6ec048f122257b9ea045dde94e6'),
(102, 'Viginesh ', 'vigineshthanikgaivasan@gmail.com', 'Chennai ', 'MIOT', NULL, NULL, '2021-05-25 20:06:26', '2021-05-25 20:06:26', '2021-05-25 21:36:26', 0, 'Abbott', '8d29fb43500f9632588310599e7a6696'),
(103, 'Arun pari', 'arunparidr@gmail.com', 'Chennai ', 'Bharath ', NULL, NULL, '2021-05-25 20:06:55', '2021-05-25 20:06:55', '2021-05-25 21:36:55', 0, 'Abbott', '95d0b6785f74e95ae3735bbf98498039'),
(104, 'Aditya', 'adityavruia@gmail.com', 'Chennai ', 'SRMC', NULL, NULL, '2021-05-25 20:07:23', '2021-05-25 20:07:23', '2021-05-25 21:37:23', 0, 'Abbott', '619087b70cc6e1f61051d6708c3247a2'),
(105, 'Ganesh', 'dr.ganesh.t.md@gmail.com', 'Chennai ', 'Evans', NULL, NULL, '2021-05-25 20:07:43', '2021-05-25 20:07:43', '2021-05-25 21:37:43', 0, 'Abbott', '718cd3a61b33d183d47b213288abf019'),
(106, 'Amit Dave', 'amit.dave@abbott.com', 'Indore', 'AV', NULL, NULL, '2021-05-25 20:08:40', '2021-05-25 20:08:40', '2021-05-25 21:38:40', 0, 'Abbott', 'bc67a0ba6fa1a015295e9a1e4b1b987b'),
(107, 'Sanjay', 'sanjay86@gmail.com', 'Chennai ', 'SIMS', NULL, NULL, '2021-05-25 20:09:16', '2021-05-25 20:09:16', '2021-05-25 21:39:16', 0, 'Abbott', '566f921338131364b6cb60bee6089608'),
(108, 'Antony', 'antonywilson@gmail.com', 'Chennai ', 'SRMC', NULL, NULL, '2021-05-25 20:09:37', '2021-05-25 20:09:37', '2021-05-25 21:39:37', 0, 'Abbott', '0c1350f69def9750901d6876e429dcf1'),
(109, 'Raman', 'raman24398@gmail.com', 'Pollachi', 'Arun hospital', NULL, NULL, '2021-05-25 20:11:51', '2021-05-25 20:11:51', '2021-05-25 21:41:51', 0, 'Abbott', '5d90d2291563f37cdbe68568bece2738'),
(110, 'Raman', 'raman24398@gmail.com', 'Pollachi', 'Arun hospital', NULL, NULL, '2021-05-25 20:12:43', '2021-05-25 20:12:43', '2021-05-25 21:42:43', 0, 'Abbott', 'a14581893664c5d6d4b7b5df9a3069c6'),
(111, 'Amit Dave', 'amit.dave@abbott.com', 'Indore', 'AV', NULL, NULL, '2021-05-25 20:13:30', '2021-05-25 20:13:30', '2021-05-25 21:43:30', 0, 'Abbott', '3857a61f79e82af4115b8082ff9fba74'),
(112, 'Shajahan', 'shajahan8121997@gmail.com', 'Kallakurichi', 'Raju hospital', NULL, NULL, '2021-05-25 20:14:21', '2021-05-25 20:14:21', '2021-05-25 21:44:21', 0, 'Abbott', 'e3382cb5f20d022b8792e1abda5f2355'),
(113, 'Dhamodharan', 'gkdhamodharan1098@gmail.com', 'RANIPET', 'GKNM HOSPITAL', NULL, NULL, '2021-05-25 20:17:38', '2021-05-25 20:17:38', '2021-05-25 21:47:38', 0, 'Abbott', 'b03a4152fc9f33da18824cbc9b24a99c'),
(114, 'Rangarao.k', 'kalirangarao@gmail.com', 'Houser', 'Sri Chandra Sekhar hospital', NULL, NULL, '2021-05-25 20:18:36', '2021-05-25 20:18:36', '2021-05-25 21:48:36', 0, 'Abbott', 'eda9b6ebe590bcd477346b1f017c26af'),
(115, 'HARDIK', 'hardik.parikh@abbott.com', 'VADODARA', 'ABBOTT', NULL, NULL, '2021-05-25 20:21:16', '2021-05-25 20:21:16', '2021-05-25 21:51:16', 0, 'Abbott', '47f73ea57e56562424e1d91b067e1f82'),
(116, 'Gowtham', 'rajigowthaman@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-05-25 20:28:55', '2021-05-25 20:28:55', '2021-05-25 21:58:55', 0, 'Abbott', 'ab898ca4f4055acaa94000a20a5eab1c'),
(117, 'Prithiviraj', 'sumiprithivi97@gmail.com', 'Coddalour', 'Surendar', NULL, NULL, '2021-05-25 20:36:44', '2021-05-25 20:36:44', '2021-05-25 22:06:44', 0, 'Abbott', '2a9bf424c4728d12e8b4591c4b225b0f'),
(118, 'Shalin', 'shalin.rajendrasinhaolanki@abbott.com', 'Vadodara', 'Av', NULL, NULL, '2021-05-25 20:42:06', '2021-05-25 20:42:06', '2021-05-25 22:12:06', 0, 'Abbott', '5218831bae1b3336d98564a6acb95c4c'),
(119, 'Ramasamy', 'ramsvision10@gmail.com', 'Coimbatore', 'PSG hospital', NULL, NULL, '2021-05-25 20:48:03', '2021-05-25 20:48:03', '2021-05-25 22:18:03', 0, 'Abbott', '2d46e979b647d2de88a2c7e470a1bf3d'),
(120, 'Daniel K', 'danielchristoper1998@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-05-26 20:34:50', '2021-05-26 20:34:50', '2021-05-26 20:35:06', 0, 'Abbott', 'b9c8dc257baaa5cb437a96a1f617d491'),
(121, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-05 12:53:10', '2021-06-05 12:53:10', '2021-06-05 12:53:21', 0, 'Abbott', '380f607316cecee61e95c37ee5385875'),
(122, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-05 12:55:28', '2021-06-05 12:55:28', '2021-06-05 12:55:55', 0, 'Abbott', '3291acfb06d5eef154b8afd05e880b0b');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=145;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
